﻿using Library.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.Repository;
using Library.DAL.EntityFramework;

namespace Library.BLL.Services
{
    public class AuthorService
    {
        public IEnumerable<Author> RequestAutocomplete(string filter)
        {
            using (var repository = new AuthorRepository())
            {
                filter = filter.ToLower();
                foreach (var author in repository.SelectAll().Where(x => x.Name.ToLower().Contains(filter)).Take(50))
                {
                    yield return author;
                }
            }
        }

        public Author AddAuthor(string name, string description)
        {
            using (var repository = new AuthorRepository())
            {
                var author = new Author() { Name = name, Description = description };
                repository.Create(author);
                return author;
            }
        }

        public void UpdateAuthor(int id, string name, string description)
        {
            using (var repository = new AuthorRepository())
            {
                var author = repository.Select(id);
                author.Name = name;
                author.Description = description;
                repository.Save();
            }
        }

        public Author RequestAuthor(int id)
        {
            var repository = new AuthorRepository();
            var authorRecord = repository.Select(id);
            if (authorRecord == null)
                return new Author() { Name = "Not found", Description = "" };

            return authorRecord;
        }

        public void RemoveAuthor(int id)
        {
            var context = new ConnectionContext("LibraryConnection");
            var repository = new AuthorInBookRepository(context);
            var matches = repository.SelectAll().Where(x => x.Author.Id == id).ToList();
            foreach (var match in matches)
                repository.Remove(match);

            using (var authorRepo = new AuthorRepository(context))
                authorRepo.Remove(authorRepo.Select(id));
        }

        const int ElementsPerPage = 15;

        public IEnumerable<Author> RequestAuthors(int page = 0)
        {
            using (var repository = new AuthorRepository())
            {
                int skip = (page - 1) * ElementsPerPage;
                return repository.SelectAmount(skip, ElementsPerPage).ToList();
            }
        }

        public IEnumerable<Author> RequestAuthorsHub()
        {
            using (var repository = new AuthorRepository())
            {
                return repository.SelectAll().ToList();
            }
        }

        public int AuthorsCountPagination()
        {
            using (var repository = new AuthorRepository())
            {
                return repository.SelectAll().Count() / ElementsPerPage + 1;
            }
        }
    }
}
