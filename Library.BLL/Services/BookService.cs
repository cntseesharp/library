﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.Repository;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.BLL.Services
{
    public class BookService
    {
        private ConnectionContext _context = null;

        const int ElementsPerPage = 15;

        public BookService(string connectionString = "LibraryConnection")
        {
            _context = new ConnectionContext(connectionString);
        }

        public void AddBook(string title, string content, List<int> authors)
        {
            ContentEntity entity = new Book()
            {
                Title = title,
                Content = content,
            };

            var repository = new BookRepository(_context);
            repository.Create(entity as Book);
            
            var authorRepo = new AuthorRepository(_context);
            var aibRepository = new AuthorInBookRepository(_context);
            foreach (var author in authors)
                aibRepository.Create(new AuthorInBooks() { Author = authorRepo.Select(author), Content = entity });

            aibRepository.Dispose();
        }

        public ContentEntity RequestBook(int id)
        {
            var repository = new BookRepository(_context);

            var contentRecord = repository.Select(id);
            if (contentRecord == null)
                return new Book() { Title = "Not Found", Content = "" };

            return contentRecord;
        }

        public void RemoveBook(int id)
        {
            Book book = null;
            var repository = new BookRepository(_context);
            book = repository.Select(id);

            var aibRepo = new AuthorInBookRepository(_context);
            var matches = aibRepo.SelectAuthors(book).ToList();
            foreach (var match in matches)
                aibRepo.Remove(match);
            repository.Remove(book);

        }

        public void UpdateBook(int id, string title = null, string content = null)
        {
            var repository = new BookRepository(_context);

            var entity = repository.Select(id);
            entity.Title = title ?? entity.Title;
            entity.Content = content ?? entity.Content;

            repository.Save();
            repository.Dispose();
        }

        public IEnumerable<Book> RequestBooksHub()
        {
            return new BookRepository().SelectAll().ToList();
        }

        public IEnumerable<ContentEntity> RequestBooks(int page = 0)
        {
            using (var repository = new BookRepository())
            {
                int skip = (page - 1) * ElementsPerPage;
                return repository.SelectAmount(skip, ElementsPerPage).ToList();
            }
        }

        public int BookCountPagination()
        {
            using (var repository = new BookRepository())
            {
                return repository.SelectAll().Count() / ElementsPerPage + 1;
            }
        }
    }
}
