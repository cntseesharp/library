﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DAL.Repository;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.BLL.Services
{
    public class ArticleService
    {
        private ConnectionContext _context = null;

        const int ElementsPerPage = 15;

        public ArticleService(string connectionString = "LibraryConnection")
        {
            _context = new ConnectionContext(connectionString);
        }

        public void AddArticle(string title, string content, List<int> authors)
        {
            ContentEntity entity = new Article()
            {
                Title = title,
                Content = content,
            };

            var repository = new ArticleRepository(_context);
            repository.Create(entity as Article);

            var authorRepo = new AuthorRepository(_context);
            var aibRepository = new AuthorInBookRepository(_context);
            foreach (var author in authors)
                aibRepository.Create(new AuthorInBooks() { Author = authorRepo.Select(author), Content = entity });

            aibRepository.Dispose();
        }

        public void UpdateContent(int id, string title = null, string content = null)
        {
            var repository = new ArticleRepository(_context);

            var entity = repository.Select(id);
            entity.Title = title ?? entity.Title;
            entity.Content = content ?? entity.Content;
            repository.Save();
            repository.Dispose();
        }

        public ContentEntity RequestArticle(int id)
        {
            var repository = new ArticleRepository(_context);

            var contentRecord = repository.Select(id);
            if (contentRecord == null)
                return new Book() { Title = "Not Found", Content = "" };

            return contentRecord;
        }
        
        public void RemoveArticle(int id)
        {
            Article article = null;
            var repository = new ArticleRepository(_context);
            article = repository.Select(id);
            repository.Remove(article);

            using (var aibRepo = new AuthorInBookRepository(_context))
            {
                var matches = aibRepo.SelectAuthors(article);
                foreach (var match in matches)
                    aibRepo.Remove(match);
            }
        }
        
        public IEnumerable<ContentEntity> RequestArticles(int page = 0)
        {
            using (var repository = new ArticleRepository())
            {
                int skip = (page - 1) * ElementsPerPage;
                return repository.SelectAmount(skip, ElementsPerPage).ToList();
            }
        }

        public IEnumerable<ContentEntity> RequestArticlesHub()
        {
            return new ArticleRepository().SelectAll().ToList();
        }

        public int ArticlesCountPagination()
        {
            using (var repository = new ArticleRepository())
            {
                return repository.SelectAll().Count() / ElementsPerPage + 1;
            }
        }
    }
}
