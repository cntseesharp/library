﻿using System.Web;
using System.Web.Optimization;

namespace Library
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/References/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/References/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/References/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/References/bootstrap.js",
                      "~/Scripts/References/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/ContentUI").Include(
                        "~/Scripts/Views/ContentUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/AuthorGrid").Include(
                        "~/Scripts/Views/AuthorGrid.js"));

            bundles.Add(new ScriptBundle("~/bundles/BookGrid").Include(
                        "~/Scripts/Views/BookGrid.js"));
            
        }
    }
}
