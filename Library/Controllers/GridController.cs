﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class GridController : Controller
    {
        // GET: Grid
        public ActionResult Author()
        {
            return View();
        }

        public ActionResult Article()
        {
            return View();
        }

        public ActionResult Book()
        {
            return View();
        }
    }
}