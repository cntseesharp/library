﻿using Library.BLL.Services;
using Library.Domain.Entities;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Library.Controllers
{
    public class AuthorsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Autocomplete(string inputValue)
        {
            var result = new List<Author>();
            foreach (var author in new AuthorService().RequestAutocomplete(inputValue).ToList())
                result.Add(new Author() { Id = author.Id, Name = author.Name, Description = author.Description });
            return Json(result);
        }

        [HttpGet]
        public Author Create(string model)
        {
            var author = JObject.Parse(model);
            return new AuthorService().AddAuthor((string)author["Name"], (string)author["Description"]);
        }

        [HttpGet]
        public void Update(string model)
        {
            var author = JObject.Parse(model);
            new AuthorService().UpdateAuthor((int)author["Id"], (string)author["Name"], (string)author["Description"]);
        }

        [HttpGet]
        public void Delete(int id)
        {
            new AuthorService().RemoveAuthor(id);
        }

        [HttpGet]
        public IHttpActionResult Read()
        {
            var list = new AuthorService().RequestAuthorsHub();
            var result = new List<Author>();
            foreach (var author in list)
                result.Add(new Author() { Id = author.Id, Name = author.Name, Description = author.Description });
            return Json(result);
        }
    }
}
