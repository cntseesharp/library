﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.BLL.Services;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class PreviewController : Controller
    {
        public ActionResult Author(int id = 0)
        {
            ViewBag.Message = "Authors.";
            return View(new AuthorService().RequestAuthor(id));
        }

        public ActionResult Book(int id = 0)
        {
            ViewBag.Message = "Books.";
            return View(new BookService().RequestBook(id));
        }

        public ActionResult Article(int id = 0)
        {
            ViewBag.Message = "Articles.";
            return View(new ArticleService().RequestArticle(id));
        }
    }
}