﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Library.BLL.Services;
using Library.Domain.Entities;
using Library.Models;
using Newtonsoft.Json.Linq;

namespace Library.Controllers
{
    public class BookController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Read()
        {
            var books = new BookService().RequestBooksHub();
            var result = new List<BookModel>();
            foreach (var book in books)
            {
                var authors = new List<string>();
                foreach (var author in book.Matches)
                    authors.Add(author.Author.Name);
                result.Add(new BookModel() { Id = book.Id, Title = book.Title, Authors = authors });
            }

            return Json(result);
        }

        [HttpGet]
        public void Update(string model)
        {
            var book = JObject.Parse(model);
            new BookService().UpdateBook((int)book["Id"], (string)book["Title"]);
        }

        [HttpGet]
        public void Delete(int id)
        {
            new BookService().RemoveBook(id);
        }
    }
}