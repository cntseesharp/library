﻿using Library.BLL.Services;
using Library.Models;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class CreateController : Controller
    {
        public ActionResult Author()
        {
            ViewBag.Message = "Add new author.";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Author(AuthorModel model)
        {
            ViewBag.Message = "Add new author.";
            if (ModelState.IsValid)
            {
                new AuthorService().AddAuthor(model.Name, model.Description);
            }
            return View(model);
        }
        
        public ActionResult Content()
        {
            ViewBag.Message = "Add new content.";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Content(ContentModel model)
        {
            ViewBag.Message = "Add new content.";
            if (ModelState.IsValid)
            {
                if(model.Type == 0)
                {
                    new BookService().AddBook(model.Title, model.Content, model.Authors);
                }
                if (model.Type == 1)
                {
                    new ArticleService().AddArticle(model.Title, model.Content, model.Authors);
                }
            }
            return View(model);
        }
    }
}