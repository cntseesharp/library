﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using Library.BLL.Services;
using Library.Models;
using System.Net;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Authors(int id = 1)
        {
            ViewBag.Message = "Authors.";
            var service = new AuthorService();
            return View(new ListingPageModel() { Authors = service.RequestAuthors(id), Page = id, PageCount = service.AuthorsCountPagination() });
        }

        public ActionResult Books(int id = 1)
        {
            ViewBag.Message = "Books.";
            var service = new BookService();
            return View(new ListingPageModel() { Content = service.RequestBooks(id), Page = id, PageCount = service.BookCountPagination() });
        }

        public ActionResult Articles(int id = 1)
        {
            ViewBag.Message = "Articles.";
            var service = new ArticleService();
            return View(new ListingPageModel() { Content = service.RequestArticles(id), Page = id, PageCount = service.ArticlesCountPagination() });
        }

        public ActionResult DBFill()
        {
            Helpers.DB.FillDB();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}