﻿using Library.DAL.EntityFramework;
using Library.DAL.Repository;
using Library.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Library.Helpers
{
    public static class DB
    {
        public static void FillDB()
        {
            var random = new Random();
            var content = new List<ContentEntity>();
            var context = new ConnectionContext("LibraryConnection");
            var bookRepo = new BookRepository(context);
            for (int i = 0; i < 100; i++)
            {
                string title = "";
                for (int j = 0; j < 15; j++)
                    title += (char)('a' + random.Next(26));
                Book b = new Book() { Content = "Book content #" + i, Title = title + " - " + i };
                bookRepo.Create(b);
                content.Add(b);
            }
            var articleRepo = new ArticleRepository(context);
            for (int i = 0; i < 100; i++)
            {
                string title = "";
                for (int j = 0; j < 15; j++)
                    title += (char)('a' + random.Next(26));
                Article b = new Article() { Content = "Book content #" + i, Title = title + " - " + i };
                articleRepo.Create(b);
                content.Add(b);
            }

            var authorsRepo = new AuthorRepository(context);
            var repo = new AuthorInBookRepository(context);
            foreach (var element in content)
                repo.Create(new AuthorInBooks()
                {
                    Author = authorsRepo.Select(random.Next(1, authorsRepo.SelectAll().Count() + 1)),
                    Content = element,
                });
            authorsRepo.Dispose();
        }
    }
}