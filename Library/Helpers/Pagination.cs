﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Library.Helpers
{
    public static class Pagination
    {
        public static string GeneratePagination(string path, int page, int totalPages)
        {
            var sb = new StringBuilder();
            sb.Append("<ul class=\"pagination\">");
            if (page > 1)
            {
                sb.Append("<li><a href=\"");
                sb.Append(path);
                sb.Append("/");
                sb.Append(page - 1);
                sb.Append("\"> < </a>");
            }

            if (page > 3)
            {
                sb.Append("<li><a href=\"");
                sb.Append(path);
                sb.Append("/1\">1</a>");
                sb.Append("<li class=\"disabled\"><a href=\"#\">...</a></li>");
            }

            if (totalPages > 1)
            {
                int start = page > 1 ? page > 2 ? page - 2 : page - 1 : page;
                int end = page < totalPages ? page + 1 < totalPages ? page + 2 : page + 1 : page;

                for (int i = start; i <= end; i++)
                {
                    sb.Append("<li");
                    if (page == i)
                        sb.Append(" class=\"active\"");
                    sb.Append("><a href=\"");
                    sb.Append(path);
                    sb.Append("/");
                    sb.Append(i);
                    sb.Append("\">");
                    sb.Append(i);
                    sb.Append("</a></li>");
                }
            }

            if (totalPages - page > 2)
            {
                sb.Append("<li class=\"disabled\"><a href=\"#\">...</a></li>");
                sb.Append("<li><a href=\"");
                sb.Append(path);
                sb.Append("/");
                sb.Append(totalPages);
                sb.Append("\">");
                sb.Append(totalPages);
                sb.Append("</a>");
            }

            if (page < totalPages)
            {
                sb.Append("<li><a href=\"");
                sb.Append(path);
                sb.Append("/");
                sb.Append(page + 1);
                sb.Append("\"> > </a>");
            }
            sb.Append("</ul>");
            return sb.ToString();
        }
    }
}