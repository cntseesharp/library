﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using Library.Domain.Entities;
using Library.BLL.Services;

namespace Library.Hubs
{
    public class AuthorHub : Hub
    {
        public IEnumerable<Author> Read()
        {
            return new AuthorService().RequestAuthorsHub();
        }

        public void Update(Author author)
        {
            new AuthorService().UpdateAuthor(author.Id, author.Name, author.Description);
            Clients.Others.update(author);
        }

        public void Destroy(Author author)
        {
            new AuthorService().RemoveAuthor(author.Id);
            Clients.Others.destroy(author);
        }

        public Author Create(Author author)
        {
            var newAuthor = new AuthorService().AddAuthor(author.Name, author.Description);
            Clients.Others.create(newAuthor);
            return newAuthor;
        }
    }
}