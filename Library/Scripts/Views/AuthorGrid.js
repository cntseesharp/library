﻿
$(function () {
    $("#notification").kendoNotification({
        width: "100%",
        position: {
            top: 0,
            left: 0
        }
    });
    $("#grid").kendoGrid({
        height: 550,
        filterable: true,
        editable: true,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        batch: false,
        columns: [
            { field: "Name", width: 200 },
            { field: "Description" },
            { command: "destroy", width: 100 }
        ],
        toolbar: ["create"],
        remove: function (obj) {
            $.ajax({
                url: "/api/Authors/Delete",
                dataType: "json",
                data: {
                    id: obj.model.Id
                }
            });
        },
        save: function (obj) {
            if (obj.values.Name != null)
                obj.model.Name = obj.values.Name;
            if (obj.values.Description != null)
                obj.model.Description = obj.values.Description;

            if (obj.model.Id != null) {
                $.ajax({
                    url: "/api/Authors/Update",
                    data: {
                        model: kendo.stringify(obj.model)
                    }
                });
            }
            if (obj.model.Id == null) {
                $.ajax({
                    url: "/api/Authors/Create",
                    data: {
                        model: kendo.stringify(obj.model)
                    },
                    success: function (result) {
                        obj.model.set("Id", result.Id);
                    }
                });
            }
        },
        dataSource: {
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: true, nullable: true },
                        Name: { type: "string" },
                        Description: { type: "string" },
                    }
                }
            },
            pageSize: 20,
            serverPaging: false,
            serverFiltering: false,
            serverSorting: false,
            transport: {
                read: {
                    url: "/api/Authors/Read"
                },
            }
        }
    });
});