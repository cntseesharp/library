﻿var requestUrl = "/api/Autocomplete/";
$(document).ready(function () {
    $("#AuthorsAC").prop("name", "AuthorsAC");
    $("#AuthorsAC").kendoAutoComplete({
        dataTextField: "Name",
        minLength: 2,
        select: function (e) {
            if (document.getElementById("Author" + this.dataItem(e.item.index()).Id) != null)
                return;

            $("#AuthorsList").append(
                "<div class=\"input-group\" id=\"Author" + this.dataItem(e.item.index()).Id + "\"><input class=\"form-control\" style=\"position: absolute;visibility: hidden;\" name=\"Authors\" type=\"text\" value=\"" + this.dataItem(e.item.index()).Id + "\">"
                +
                "<span class=\"input-group-btn\"><button class=\"btn btn-default\" type=\"button\" onclick=\"remove(this);\">X</button></span><input class=\"form-control\" readonly type=\"text\" value=\"" + this.dataItem(e.item.index()).Name + "\"></div>");
        },
        dataSource: new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    $.ajax({
                        url: "/api/Authors/Autocomplete/" + $("#AuthorsAC").val(),
                        dataType: "json",
                        success: function (result) {
                            options.success(result);
                        },
                        error: function (result) {
                            options.error(result);
                        }
                    });
                },
            }
        })
    });
});
function remove(element) {
    element.parentElement.parentElement.remove();
}