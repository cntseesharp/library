﻿$(function () {
    $("#notification").kendoNotification({
        width: "100%",
        position: {
            top: 0,
            left: 0
        }
    });
    $("#grid").kendoGrid({
        height: 550,
        filterable: true,
        editable: true,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        batch: false,
        columns: [
            { field: "Title", width: 200 },
            { field: "Authors", title: "Authors", template: "#= Authors.join(', ') #" },
            { command: { text: "Details", click: showDetails, className: "btn-eye" }, width: 100 },
            { command: "destroy", width: 100 }
        ],
        toolbar: [{
            text: "Add new book",
            className: "k-grid-custom",
            imageClass: "k-icon k-i-plus",
        }],
        dataBound: function (e) {
            var span = $('<span></span>').addClass('k-icon k-i-eye');
            $('.k-grid .btn-eye').prepend(span);
        },
        remove: function (obj) {
            $.ajax({
                url: "/api/Book/Delete",
                dataType: "json",
                data: {
                    id: obj.model.Id
                }
            });
        },
        save: function (obj) {
            if (obj.values.Title != null)
                obj.model.Title = obj.values.Title;

            if (obj.model.Id != null) {
                $.ajax({
                    url: "/api/Book/Update",
                    data: {
                        model: kendo.stringify(obj.model)
                    }
                });
            }
        },
        dataSource: {
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        Id: { editable: false, nullable: true },
                        Title: { type: "string" },
                        Authors: { type: "object", editable: false },
                    }
                }
            },
            pageSize: 20,
            serverPaging: false,
            serverFiltering: false,
            serverSorting: false,
            transport: {
                read: {
                    url: "/api/Book/Read"
                },
            }
        }
    });
});
$(document).ready(function () {
    $('.k-grid-custom').prepend($('<span></span>').addClass('k-icon k-i-plus'));
    $('.k-grid-custom').click(function () {
        window.open("/Create/Content", '_blank');
    });
});

function showDetails(obj) {
    obj.preventDefault();

    var dataItem = this.dataItem($(obj.currentTarget).closest("tr"));
    window.open("/Preview/Book/" + dataItem.Id, '_blank');
}