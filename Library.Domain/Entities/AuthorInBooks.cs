﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Domain.Entities
{
    public class AuthorInBooks
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }
        [Key, Column(Order = 1)]
        public virtual Author Author { get; set; }
        [Key, Column(Order = 2)]
        public virtual ContentEntity Content { get; set; }
    }
}
