﻿using System.Collections.Generic;
using System.Linq;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.DAL.Repository
{
    public class BookRepository : RepositoryBase<Book>
    {
        public BookRepository(ConnectionContext db = null) : base(db)
        {
        }

        public override void Create(Book element)
        {
            Database.Books.Add(element);
            Database.SaveChanges();
        }

        public override void Remove(Book element)
        {
            Database.Books.Remove(element);
            Database.SaveChanges();
        }

        public override Book Select(int id)
        {
            return Database.Books.FirstOrDefault(x => x.Id == id);
        }

        public override IEnumerable<Book> SelectAll()
        {
            return Database.Books;
        }

        public override IEnumerable<Book> SelectAmount(int skip, int count)
        {
            return Database.Books.OrderBy(x => x.Title).Skip(skip).Take(count);
        }
    }
}
