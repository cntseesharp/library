﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.DAL.Repository
{
    public class AuthorInBookRepository : RepositoryBase<AuthorInBooks>
    {
        public AuthorInBookRepository(ConnectionContext db = null) : base(db)
        {
        }

        public override void Create(AuthorInBooks element)
        {
            Database.AuthorInBook.Add(element);
            Database.SaveChanges();
        }

        public override void Remove(AuthorInBooks element)
        {
            Database.AuthorInBook.Remove(element);
            Database.SaveChanges();
        }

        public IEnumerable<AuthorInBooks> SelectBooks(Author author)
        {
            return Database.AuthorInBook.Where(x => x.Author.Id == author.Id && x.Content is Book);
        }

        public IEnumerable<AuthorInBooks> SelectArticles(Author author)
        {
            return Database.AuthorInBook.Where(x => x.Author.Id == author.Id && x.Content is Article);
        }

        public IEnumerable<AuthorInBooks> SelectAuthors(ContentEntity entity)
        {
            return Database.AuthorInBook.Where(x => x.Content.Id == entity.Id);
        }

        public override IEnumerable<AuthorInBooks> SelectAll()
        {
            return Database.AuthorInBook;
        }

        public IEnumerable<AuthorInBooks> SelectAll(Author author)
        {
            return Database.AuthorInBook.Where(x => x.Author.Id == author.Id);
        }

        public override AuthorInBooks Select(int id)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<AuthorInBooks> SelectAmount(int skip, int count)
        {
            throw new NotImplementedException();
        }
    }
}
