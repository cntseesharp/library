﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public interface IRepository<T> : IDisposable
    {
        void Create(T element);
        void Remove(T element);
        T Select(int id);
        IEnumerable<T> SelectAmount(int skip, int count);
        IEnumerable<T> SelectAll();
        void Save();
    }
}
