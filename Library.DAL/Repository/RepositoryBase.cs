﻿using System;
using System.Collections.Generic;
using Library.DAL.EntityFramework;

namespace Library.DAL.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T>
    {
        protected ConnectionContext Database { get; set; }
        public RepositoryBase(ConnectionContext db = null)
        {
            Database = db ?? new ConnectionContext("LibraryConnection");
        }
        
        public abstract void Create(T element);
        public abstract void Remove(T element);
        public abstract T Select(int id);
        public abstract IEnumerable<T> SelectAmount(int skip, int count);
        public abstract IEnumerable<T> SelectAll();

        public void Dispose()
        {
            Database.Dispose();
        }

        public void Save()
        {
            Database.SaveChanges();
        }
    }
}
