﻿using System.Collections.Generic;
using System.Linq;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.DAL.Repository
{
    public class ArticleRepository : RepositoryBase<Article>
    {
        public ArticleRepository(ConnectionContext db = null) : base(db)
        {
        }

        public override void Create(Article element)
        {
            Database.Articles.Add(element);
            Database.SaveChanges();
        }

        public override void Remove(Article element)
        {
            Database.Articles.Remove(element);
            Database.SaveChanges();
        }

        public override Article Select(int id)
        {
            return Database.Articles.FirstOrDefault(x => x.Id == id);
        }

        public override IEnumerable<Article> SelectAll()
        {
            return Database.Articles;
        }

        public override IEnumerable<Article> SelectAmount(int skip, int count)
        {
            return Database.Articles.OrderBy(x => x.Title).Skip(skip).Take(count);
        }
    }
}
