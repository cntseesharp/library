﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.DAL.EntityFramework;
using Library.Domain.Entities;

namespace Library.DAL.Repository
{
    public class AuthorRepository : RepositoryBase<Author>
    {
        public AuthorRepository(ConnectionContext db = null) : base(db)
        {
        }

        public override void Create(Author element)
        {
            Database.Authors.Add(element);
            Database.SaveChanges();
        }

        public override void Remove(Author element)
        {
            Database.Authors.Remove(element);
            Database.SaveChanges();
        }

        public override Author Select(int id)
        {
            return Database.Authors.FirstOrDefault(x => x.Id == id);
        }

        public override IEnumerable<Author> SelectAll()
        {
            return Database.Authors;
        }

        public override IEnumerable<Author> SelectAmount(int skip, int count)
        {
            return Database.Authors.OrderBy(x => x.Name).Skip(skip).Take(count);
        }
    }
}
