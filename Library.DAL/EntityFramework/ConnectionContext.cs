﻿using Library.Domain.Entities;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Library.DAL.EntityFramework
{
    public class ConnectionContext : DbContext
    {
        public ConnectionContext(string conectionString) : base(conectionString)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ConnectionContext>());
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<AuthorInBooks> AuthorInBook { get; set; }
    }

    public class MigrationsContextFactory : IDbContextFactory<ConnectionContext>
    {
        public ConnectionContext Create()
        {
            return new ConnectionContext("LibraryConnection");
        }
    }
}