﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Models
{
    class ArticleModel
    {
        [Required, Display(Description = "Content title")]
        public string Title { get; set; }
        [Required, Display(Description = "Content authors")]
        public List<int> Authors { get; set; }
        [Required, Display(Description = "Content itself")]
        public string Content { get; set; }
        [Required, Display(Description = "Content type")]
        public int Type { get; set; }
    }
}
