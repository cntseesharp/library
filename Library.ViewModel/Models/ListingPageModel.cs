﻿using Library.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class ListingPageModel
    {
        public IEnumerable<Author> Authors { get; set; }
        public IEnumerable<ContentEntity> Content { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }
}