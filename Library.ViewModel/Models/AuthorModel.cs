﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class AuthorModel
    {
        [Required, Display(Description = "Authors name and surname")]
        public string Name { get; set; }
        [Display(Description = "Describe author")]
        public string Description { get; set; }
    }
}